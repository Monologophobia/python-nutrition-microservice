import configparser

def load_config():
    """ loads the settings files from config.ini """

    config = configparser.ConfigParser()
    config.read('config.ini')

    if not "nutrition" in config:
        print('Config file missing information')
        sys.exit()

    return config