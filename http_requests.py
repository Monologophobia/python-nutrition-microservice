import json
from urllib.parse import urlsplit, parse_qs, urlparse
from http.server import BaseHTTPRequestHandler, HTTPServer

class http_requests(BaseHTTPRequestHandler):

    def do_GET(self):
        """ GET lists food items by limit 30, specified by page number and food id """

        # parse_qs comes through as a list so select first element

        name = parse_qs(urlparse(self.path).query).get('name', None)
        name = name[0] if name else None
        
        page = parse_qs(urlparse(self.path).query).get('page')
        page = int(page[0]) if page else 1
        limit = parse_qs(urlparse(self.path).query).get('limit')
        limit = int(limit[0]) if limit else 30

        self.send(self.database.list_items(name, limit, page))

    def do_POST(self):

        # get the data
        self.data = self.get_data()

        if not self.authorised():
            print('here')
            self.send_error(401)
            return

        name = self.get_data_by_key('name')
        barcode = self.get_data_by_key('barcode')
        food_code = self.get_data_by_key('food_code')

        # if the query is not allowed, return an error
        if not name and not barcode and not food_code:
            self.send_error(400)
            return

        item = False

        if name:
            # if the query length is less than 3, return error
            if len(name) < 3:
                self.send_error(400)
                return
            item = self.database.find_item(name)

        # if the query is for a barcode
        if barcode:
            item = self.database.find_item_by_barcode(barcode)

        # if the query is for a specific food code
        if food_code:
            item = self.database.find_item_by_food_code(food_code)

        self.send(item)

    def do_PUT(self):

        # get the data
        self.data = self.get_data()

        if not self.authorised():
            self.send_error(401)
            return

        task = self.get_data_by_key('task')
        if task not in ('user', 'food'):
            self.send_error(400)
            return

        if task == "user":
            if self.super_user():
                email = self.get_data_by_key('email')
                token = self.database.create_user(email)
                self.send({"token": token})

        if task == "food":
            food_code = self.get_data_by_key('food_code')
            name = self.get_data_by_key('name')
            calories = int(self.get_data_by_key('calories'))
            carbohydrates = float(self.get_data_by_key('carbohydrates'))
            protein = float(self.get_data_by_key('protein'))
            fat = float(self.get_data_by_key('fat'))
            barcode = self.get_data_by_key('barcode')
            self.database.put_food(name, calories, protein, fat, carbohydrates, barcode, food_code)
            self.send({"success": True})

    def do_DELETE(self):

        # get the data
        self.data = self.get_data()

        if not self.authorised():
            self.send_error(401)
            return

        task = self.get_data_by_key('task')
        if task not in ('user'):
            self.send_error(400)
            return

        if task == "user":
            # only the master user can delete other users
            if self.super_user():
                token = self.get_data_by_key('token')
                email = self.get_data_by_key('email')
                result = self.database.delete_user(token, email)
                self.send({"deleted": result})
            else:
                self.send_error(401)
                return

    def authorised(self):
        """ Checks the authorisation headers for a match """

        if self.super_user():
            return True
        else:
            token = self.get_data_by_key('token')
            email = self.get_data_by_key('email')
            if not token or not email:
                return False
            return self.database.check_user(token, email)

    def super_user(self):
        master_token = self.get_data_by_key('master_token')
        master_email = self.get_data_by_key('master_email')
        if not master_token or not master_email:
            return False
        if master_token == self.config['nutrition']['master_token']:
            if master_email == self.config['nutrition']['master_email']:
                return True
        return False

    def get_data(self):
        """ gets the data from the initial request """
        # get the data (get size of content to read through)
        data = self.rfile.read(int(self.headers['Content-Length']))
        data = data.decode("utf-8")
        return parse_qs(data)

    def get_data_by_key(self, query_key):
        """ gets the query_key from the post'd data """
        try:
            # post data is returned as a list in a dict so get the first list item
            return self.data.get(query_key)[0]
        except:
            return None

    def send(self, json_data):
        """ sends a 200 response with a json body """
        self.send_response(200)
        self.send_header('Content-type', 'text/json')
        self.end_headers()
        # wfile needs bytes or string
        data = json.dumps(json_data).encode("utf-8")
        self.wfile.write(data)
