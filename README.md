# Nutritional Information Python Microservice

This python script listens for string requests for a specific food item and returns nutritional information. The script allows wildcard searches as well so requesting "hicke" will return all food instances with "chicken" in the name.

## Data origins
The service includes an import function for data supplied by [gov.uk](https://www.gov.uk/government/publications/composition-of-foods-integrated-dataset-cofid)

## Quickstart
`$ cp config.ini.example config.ini`

`$ python3 main.py &`

For a name search - 
`$ curl http://127.0.0.1:5000 --data "name=YOUR-SEARCH-TERM&email=USER-EMAIL&token=USER-TOKEN"`

For a barcode search -
`$ curl http://127.0.0.1:5000 --data "barcode=YOUR-SEARCH-TERM&email=USER-EMAIL&token=USER-TOKEN"`

For a specific food code search
`$ curl http://127.0.0.1:5000 --data "food_code=YOUR-SEARCH-TERM&email=USER-EMAIL&token=USER-TOKEN"`

### Create a user
`$ curl http://127.0.0.1:5000 -X PUT --data "task=user&email=NEW@EMAIL.COM&master_email=MASTER-EMAIL&master_token=MASTER-TOKEN"`
Returns {"token": string}

### Delete a user
`$ curl http://127.0.0.1:5000 -X DELETE --data "task=user&email=USERS-EMAIL&token=USERS-TOKEN&master_email=MASTER-EMAIL&master_token=MASTER-TOKEN"`
Returns {"deleted": boolean}

## Import data
Download .xlsx file from https://www.gov.uk/government/publications/composition-of-foods-integrated-dataset-cofid
`$ python3 main.py --import filename.xslx`
