import sqlite3
import random

class database:

    def __init__(self):

        # sqlite3 connect automatically creates the file if it doesn't exist
        self.connection = sqlite3.connect('./database/database.sqlite')

        self.version = 2
        self.check_database_version()

    def check_database_version(self):
        """ check whether the database is the version we want """
        cursor = self.connection.cursor()
        cursor.execute("PRAGMA user_version;")
        version = cursor.fetchone()[0]
        if version < self.version:
            self.update_database(version)

    def update_database(self, version):
        """ updates the database with the necessary information """

        if version < 1:
            # if this is version 1, create the database
            self.setup_database()
        if version < 2:
            # version 2 introduces basic authentication
            self.create_users_table()

        self.set_version(self.version)

    def setup_database(self):
        """ creates the database tables """

        cursor = self.connection.cursor()

        # create the main product database
        cursor.execute(
            "CREATE TABLE food_items ("
	            "id	INTEGER PRIMARY KEY, "
                "food_code TEXT NOT NULL UNIQUE, "
	            "name TEXT NOT NULL COLLATE NOCASE, "
                "calories NUMERIC, "
                "protein NUMERIC, "
                "fat NUMERIC, "
                "carbohydrates NUMERIC, "
                "barcode TEXT"
            ");")

        # create indexes
        cursor.execute("CREATE INDEX barcode on food_items(barcode)")
        cursor.execute("CREATE INDEX food_code on food_items(barcode)")

        # create virtual table for full text searches
        # https://sqlite.org/fts5.html
        cursor.execute("CREATE VIRTUAL TABLE food_items_names USING fts4(food_id INTEGER NOT NULL, name TEXT NOT NULL COLLATE NOCASE);")

        # create custom triggers to automate virtual table population
        # delete
        cursor.execute(
            "CREATE TRIGGER delete_item AFTER DELETE ON food_items "
                "BEGIN "
                    "DELETE FROM food_items_names WHERE food_id = old.id;"
                "END;"
        )
        # update
        cursor.execute(
            "CREATE TRIGGER update_name AFTER UPDATE OF name ON food_items "
                "BEGIN "
                    "UPDATE food_items_names SET name = new.name WHERE food_id = old.id; "
                "END;"
        )
        # create
        cursor.execute(
            "CREATE TRIGGER insert_item AFTER INSERT ON food_items "
                "BEGIN "
                    "INSERT INTO food_items_names(food_id, name) VALUES(new.id, new.name);" 
                "END;"
        )

        self.connection.commit()

    def create_users_table(self):
        """ creates the users tables. This allows basic auth over https with simple tokens """

        cursor = self.connection.cursor()
        cursor.execute(
            "CREATE TABLE users ("
	            "id	INTEGER PRIMARY KEY, "
                "token TEXT NOT NULL UNIQUE, "
                "email TEXT NOT NULL UNIQUE"
            ");")
        self.connection.commit()

    def set_version(self, version):
        """ sets the database version number """
        cursor = self.connection.cursor()
        cursor.execute("PRAGMA user_version = %d;" % version)
        self.connection.commit()

    def import_data(self, data):
        """ imports an array of dictionary items """
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM food_items;")
        for item in data:
            cursor.execute(
                "INSERT INTO food_items(food_code, name, calories, protein, fat, carbohydrates, barcode) VALUES (?, ?, ?, ?, ?, ?, ?)",
                (item["food_code"], item["name"], item["calories"], item["protein"], item["fat"], item["carbohydrates"], item["barcode"])
            )
        self.connection.commit()

    def list_items(self, name = False, limit = 30, page = 1):
        """ lists items in groups of 30
            Can supply name to search or leave blank
            Can supply page to start at or defaults to 1 """

        if name:
            return self.find_item(name, limit, page)
        else:
            offset = (page - 1) * limit
            cursor = self.connection.cursor()
            cursor.execute("SELECT * FROM food_items_names LIMIT ? OFFSET ?;", (limit, offset, ))
            results = cursor.fetchall()
            return self.get_items_from_results(results)

    def find_item(self, query_string, limit = 10, page = 1):
        """ finds an item based on the query string """

        cursor = self.connection.cursor()

        offset = (page - 1) * limit

        # check the query string against the FTS virtual table (collate nocase as we can't set that on table creation)
        # first, do a full word match
        cursor.execute("SELECT food_id FROM food_items_names WHERE name MATCH ? COLLATE NOCASE LIMIT ? OFFSET ?;", (query_string, limit, offset, ))
        results = cursor.fetchall()
        if results:
            return self.get_items_from_results(results)
        # if not, try a slower wildcard search
        else:
            cursor.execute("SELECT food_id FROM food_items_names WHERE name LIKE ? COLLATE NOCASE LIMIT ? OFFSET ?;", ('%' + query_string + '%', limit, offset, ))
            results = cursor.fetchall()
            if results:
                return self.get_items_from_results(results)
            # if not, try a barcode match
            else:
                return self.find_item_by_barcode(query_string)

        return False

    def find_item_by_barcode(self, barcode):
        """ finds an item by a specific barcode string """
        cursor = self.connection.cursor()
        cursor.execute("SELECT id FROM food_items WHERE barcode = ? LIMIT 1;", (barcode, ))
        results = cursor.fetchall()
        return self.get_items_from_results(results) if results else False

    def find_item_by_food_code(self, food_code):
        """ finds an item by a specific food code string """
        cursor = self.connection.cursor()
        cursor.execute("SELECT id FROM food_items WHERE food_code = ? LIMIT 1;", (food_code, ))
        results = cursor.fetchall()
        return self.get_items_from_results(results) if results else False

    def get_items_from_results(self, results):
        """ gets a list of products and their details from the supplied array """

        cursor = self.connection.cursor()

        # create a nice array of ids
        ids = []
        for item in results:
            ids.append(item[0])

        placeholders = '?'
        placeholders = ', '.join(placeholders for id in ids)

        query= "SELECT food_code, name, calories, protein, fat, carbohydrates, barcode FROM food_items WHERE id IN (%s)" % placeholders
        cursor.execute(query, ids)

        results = []
        for food_code, name, calories, protein, fat, carbohydrates, barcode in cursor.fetchall():
            item = {
                'food_code': food_code,
                'name': name,
                'calories': calories,
                'protein': protein,
                'fat': fat,
                'carbohydrates': carbohydrates,
                'barcode': barcode
            }
            results.append(item)

        return results

    def put_food(self, name, calories, protein, fat, carbohydrates, barcode, food_code = False):
        """ Creates or Updates a food item """
        try:
            cursor = self.connection.cursor()
            if food_code:

                # make sure food_code is valid
                item = self.find_item_by_food_code(food_code)
                if not item:
                    return False

                query = "UPDATE food_items SET name = ?, calories = ?, protein = ?, fat = ?, carbohydrates = ?, barcode = ? WHERE food_code = ?"
                cursor.execute(query, (name, calories, protein, fat, carbohydrates, barcode, food_code, ))
                self.connection.commit()

                return True

            else:

                # first, insert the item with a random food code
                query = "INSERT INTO food_items (food_code, name, calories, protein, fat, carbohydrates, barcode) VALUES (?, ?, ?, ?, ?, ?, ?)"
                cursor.execute(query, (random.randint(0, 9999), name, calories, protein, fat, carbohydrates, barcode, ))

                # once we've inserted it, we should have the autoincrement id
                row_id = cursor.lastrowid
                cursor.execute("UPDATE food_items SET food_code = ? WHERE id = ?", (row_id, row_id, ))

                self.connection.commit()

                return True

        except Exception as e:
            return False

    def create_user(self, email):
        """ creates a user and returns a new key """
        try:
            # generate random token
            import os
            from base64 import b64encode
            token = os.urandom(16)
            token = b64encode(token).decode('utf-8')

            cursor = self.connection.cursor()
            cursor.execute("INSERT OR REPLACE INTO users (token, email) VALUES (?, ?)", (token, email, ))
            self.connection.commit()

            return token

        except Exception as e:
            return False

    def check_user(self, token, email):
        """ checks the given token and email for a matching user """
        cursor = self.connection.cursor()
        query = "SELECT * FROM users WHERE token = ? AND email = ? LIMIT 1;"
        cursor.execute(query, (token, email, ))
        return cursor.fetchone()

    def delete_user(self, token, email):
        """ deletes a user with a matching token and email """
        try:
            cursor = self.connection.cursor()
            query = "DELETE FROM users WHERE token = ? AND email = ?;"
            cursor.execute(query, (token, email, ))
            self.connection.commit()
            # eh. this will always return true...
            return True
        except:
            return False
