import sys

from database import database
from configuration import load_config

def import_data(arguments):
    """ imports xlsx from https://www.gov.uk/government/publications/composition-of-foods-integrated-dataset-cofid """

    config = load_config()
    config = config["import"]

    if not len(arguments) > 1:
        print ('Please specify an .xlsx file to import')
        sys.exit()

    # import the xlrd library. If it doesn't exists, use pip to install it
    try:
        import xlrd
    except ImportError:
        # install it and try again
        import pip
        pip.main(['install', 'xlrd', '--user'])
        import_data(arguments)

    try:
        print("Importing Data...")

        # filename, ondemand (so we don't load the whole thing and only the sheet we want)
        wb    = xlrd.open_workbook(filename = arguments[1], on_demand = True)
        sheet = wb.sheet_by_name("Proximates")

        # get the significant rows, columns, and compile it all in to an array
        row     = int(config['data_starts_at_row_number']) if config['data_starts_at_row_number'] else 2
        columns = load_column_numbers(config)
        data    = get_data(sheet, row, columns)

        ## and save that array to the database
        db = database()
        db.import_data(data)

    except FileNotFoundError:
        print('Import file not found. Please check and try again.')
        sys.exit()

    except xlrd.biffh.XLRDError:
        print('Data does not contain the Proximates sheet')
        sys.exit()

def load_column_numbers(config):
    """ Load starting row and columns from the config """
    
    return {
        "food_code": int(config['column_food_code']) if config['column_food_code'] else 0,
        "name": int(config['column_name']) if config['column_name'] else 1,
        "calories": int(config['column_calories']) if config['column_calories'] else 12,
        "protein": int(config['column_protein']) if config['column_protein'] else 9,
        "fat": int(config['column_fat']) if config['column_fat'] else 10,
        "carbohydrates": int(config['column_carbohydrates']) if config['column_carbohydrates'] else 11
    }

def get_data(sheet, row, columns):
    """ iterate over sheet row length and compile data in to an array of dictionary objects """

    data = []

    while row < sheet.nrows:
        item = {
            "food_code": sheet.cell_value(row, columns["food_code"]),
            "name": sheet.cell_value(row, columns["name"]),
            "calories": sheet.cell_value(row, columns["calories"]),
            "protein": sheet.cell_value(row, columns["protein"]),
            "fat": sheet.cell_value(row, columns["fat"]),
            "carbohydrates": sheet.cell_value(row, columns["carbohydrates"]),
            "barcode": False
        }
        data.append(item)
        row += 1

    return data
