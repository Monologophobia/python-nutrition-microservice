import os
import sys

from http.server import BaseHTTPRequestHandler, HTTPServer

from database import database
from import_data import import_data
from configuration import load_config
from http_requests import http_requests

class http_server:
    def __init__(self, config):

        # sets up the host/port and secret token
        host  = config['nutrition']['host']
        port  = int(config['nutrition']['port'])

        # set up config and database
        http_requests.database = database()
        http_requests.config = config

        server = HTTPServer((host, port), http_requests)
        print("Listening on {}:{}".format(host, port))
        server.serve_forever()

def main():
    """ Listen on a host/port for requests """

    config = load_config()
    http_server(config)

if __name__ == '__main__':

    # when running from /usr/bin/python3 /whatever/this/is/main.py make sure the correct cwd is used
    os.chdir(sys.path[0])

    arguments = sys.argv[1:]
    if arguments and arguments[0] == "--import":
        import_data(arguments)

    main()
